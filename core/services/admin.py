from django.contrib import admin

from .models import Services, ServiceItems


class ServiceItemsInline(admin.TabularInline):
    model = ServiceItems


@admin.register(Services)
class ServicesAdmin(admin.ModelAdmin):
    fields = (
        'image',
        'title',
        'description',
    )
    inlines = (ServiceItemsInline,)


@admin.register(ServiceItems)
class ServiceItemsAdmin(admin.ModelAdmin):
    fields = (
        'services',
        'description',
        'price'
    )