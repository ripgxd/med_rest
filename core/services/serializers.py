from django.db.models import fields
from rest_framework import serializers

from .models import Services, ServiceItems


class ServiceItemsSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ServiceItems
        fields = ('__all__')


class ServicesSerializer(serializers.ModelSerializer):
    
    image = serializers.ImageField(read_only=True)
    services = ServiceItemsSerializer(many=True)

    class Meta:
        model = Services
        fields = [
            'id',
            'image',
            'title',
            'description',
            'services'
        ]

