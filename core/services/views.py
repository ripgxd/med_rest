from rest_framework import generics

from .serializers import ServicesSerializer
from .models import Services


class ServicesListView(generics.ListCreateAPIView):
    queryset = Services.objects.all()
    serializer_class = ServicesSerializer
