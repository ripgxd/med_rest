from django.db import models
from utils.models import TimeStamp, ActiveOrder

from ckeditor_uploader.fields import RichTextUploadingField


class Services(
    TimeStamp, 
    ActiveOrder, 
    models.Model
    ):

    image = models.ImageField()

    title = models.CharField(
        max_length=40, 
        verbose_name='Послуга'
    )

    description = RichTextUploadingField(verbose_name='Опис')

    class Meta:
        verbose_name = 'Послуга'
        verbose_name_plural = 'Послуги'
        ordering = ['order']

    def __str__(self):
        return self.title


class ServiceItems(
    TimeStamp, 
    ActiveOrder, 
    models.Model
    ):

    services = models.ForeignKey(
        Services, 
        on_delete=models.CASCADE, 
        verbose_name='Послуга', 
        related_name='services'
    )

    description = RichTextUploadingField(verbose_name='Опис')

    price = models.CharField(
        max_length=30, 
        verbose_name='Ціна'
    )

    class Meta:
        verbose_name = 'Опис послуги'
        verbose_name_plural = 'Опис послуг'
        ordering = ['services']

    def __str__(self):
        return self.description