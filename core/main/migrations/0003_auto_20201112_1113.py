# Generated by Django 3.1.3 on 2020-11-12 09:13

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_auto_20201112_1042'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='sitesettings',
            options={'verbose_name': 'Налаштування', 'verbose_name_plural': 'Налаштування'},
        ),
    ]
