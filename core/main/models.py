from django.db import models


class SingletonModel(models.Model):
    class Meta:
        abstract = True
 
    def save(self, *args, **kwargs):
        self.__class__.objects.exclude(id=self.id).delete()
        super(SingletonModel, self).save(*args, **kwargs)
 
    @classmethod
    def load(cls):
        try:
            return cls.objects.get()
        except cls.DoesNotExist:
            return cls()

class SiteSettings(SingletonModel):
    title = models.CharField(
        verbose_name='Title', 
        max_length=256,
        null=True
        )
    background = models.ImageField(
        verbose_name='Фон на головній сторінці',
        null=True
    )
    license = models.TextField(
        verbose_name='Ліцензія',
        null=True
    )
 
    class Meta:
        verbose_name="Налаштування"
        verbose_name_plural="Налаштування"

    def __str__(self):
        return 'Налаштування'    