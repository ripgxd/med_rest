from django.urls import path

from .views import (
    AppointmentAPIView,
    SocialAPIView,
    MarkersAPIView,
    ContactsAPIView
)


urlpatterns = [
    path('appointment/', AppointmentAPIView.as_view(), name='appointment'),
    path('social/', SocialAPIView.as_view(), name='social'),
    path('markers/', MarkersAPIView.as_view(), name='markers'),
    path('contacts/', ContactsAPIView.as_view(), name='contacts'),
]