from rest_framework import serializers

from .models import (
    Appointment,
    Social,
    Marker,
    Contacts
)


class AppointmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Appointment
        fields = (
            'name',
            'phone',
            'additional',
        )


class SocialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Social
        fields = ('__all__',)


class MarkersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Marker
        fields = ('__all__',)


class ContactsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contacts
        fields = ('__all__',)