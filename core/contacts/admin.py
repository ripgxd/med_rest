from django.contrib import admin

from .models import (
    Appointment,
    Social,
    Marker,
    Contacts
)


@admin.register(Appointment)
class AppointmentAdmin(admin.ModelAdmin):
    fields = (
        'name',
        'phone',
        'additional'
    )


@admin.register(Social)
class SocialAdmin(admin.ModelAdmin):
    fields = (
        'name',
        'url'
    )


@admin.register(Marker)
class MarkerAdmin(admin.ModelAdmin):
    fields = (
        'latitude',
        'longitude'
    )


@admin.register(Contacts)
class ContactsAdmin(admin.ModelAdmin):
    fields = (
        'address',
        'email',
        'first_phone',
        'second_phone',
        'schedule'
    )