from rest_framework import generics

from .models import (
    Appointment,
    Social,
    Marker,
    Contacts
)
from .serializers import (
    AppointmentSerializer,
    SocialSerializer,
    MarkersSerializer,
    ContactsSerializer
)


class AppointmentAPIView(generics.CreateAPIView):
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer


class SocialAPIView(generics.ListAPIView):
    queryset = Social.objects.all()
    serializer_class = SocialSerializer


class MarkersAPIView(generics.ListAPIView):
    queryset = Marker.objects.all()
    serializer_class = MarkersSerializer


class ContactsAPIView(generics.ListAPIView):
    queryset = Contacts.objects.all()
    serializer_class = ContactsSerializer