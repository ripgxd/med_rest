from django.urls import path

from .views import (
   ClinicAPIView,
   DoctorAPIView,
   GalleryAPIView
)


urlpatterns = [
    path('clinic/', ClinicAPIView.as_view(), name='clinic'),
    path('doctors/', DoctorAPIView.as_view(), name='doctors'),
    path('gallery/', GalleryAPIView.as_view(), name='gallery'),
]