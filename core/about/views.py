from rest_framework import generics

from .models import (
    Clinic,
    Doctor,
    Gallery
)

from .serializers import (
    ClinicSerializer,
    DoctorSerializer,
    GallerySerializer
)


class ClinicAPIView(generics.ListAPIView):
    queryset = Clinic.objects.all()
    serializer_class = ClinicSerializer


class DoctorAPIView(generics.ListAPIView):
    queryset = Doctor.objects.all()
    serializer_class = DoctorSerializer


class GalleryAPIView(generics.ListAPIView):
    queryset = Gallery.objects.all()
    serializer_class = GallerySerializer
