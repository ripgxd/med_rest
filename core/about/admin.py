from django.contrib import admin

from .models import (
    Clinic,
    Doctor,
    Gallery
)


@admin.register(Clinic)
class ClinicAdmin(admin.ModelAdmin):
    fields = (
        'title',
        'description',
        'doctor'
    )


@admin.register(Doctor)
class DoctorAdmin(admin.ModelAdmin):
    fields = (
        'image',
        'surname',
        'name',
        'middle_name',
        'description'
    )


@admin.register(Gallery)
class GalleryAdmin(admin.ModelAdmin):
    fields = ('image',)