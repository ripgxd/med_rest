from rest_framework import serializers
from .models import News


class NewsListSerializer(serializers.ModelSerializer):

    image = serializers.ImageField(read_only=True)

    class Meta: 
        model = News
        fields = [
            'id', 
            'image', 
            'title', 
            'description'
        ]

class NewsDetailSerializer(serializers.ModelSerializer):

    image = serializers.ImageField(read_only=True)

    class Meta: 
        model = News
        fields = [
            'id', 
            'image', 
            'title', 
            'description', 
            'created_date', 
            'updated_date', 
            'is_active', 
            'order'
        ]