from django.urls import path

from .views import NewsDetailList, NewsList


urlpatterns = [
    path('news/', NewsList.as_view(), name='news-list'),
    path('news/<int:id>/', NewsDetailList.as_view(), name='news-detail'),
]