from rest_framework import generics

from .models import News
from .serializers import NewsListSerializer, NewsDetailSerializer


class NewsList(generics.ListCreateAPIView):
    
    queryset = News.objects.all()
    serializer_class = NewsListSerializer


class NewsDetailList(generics.RetrieveUpdateDestroyAPIView):

    lookup_url_kwarg = 'id'
    queryset = News.objects.all()
    serializer_class = NewsDetailSerializer